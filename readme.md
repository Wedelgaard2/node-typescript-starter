# Starter template for Node.js applications written in TypeScript

## Setup

This project requires that you have npm and node installed. Clone project and run `npm init`

### Development

`npm run start`

### Production

`npm run build`
